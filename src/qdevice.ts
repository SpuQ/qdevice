/*
 *  Qdevice
 *  For using the QCom USB device interface
 *
 *  Copyright 2022 Sanne 'SpuQ' Santens. All rights reserved.
 */

import {EventEmitter} from 'events';
import * as readline from 'readline';
import * as net from 'net';
import { existsSync } from 'fs';

export interface Qdata {
    signal: string,         // Signal
    argument: string        // argument
}

export class Qcom extends EventEmitter {
	socketPath:string;      // The path to the UNIX socket
	socket:any;             // the socket
	connected = false;      // Device connection status

    constructor( deviceName:string ){
        super();
        // Set the path to the device socket file
        this.socketPath = "/Qdev/"+deviceName;
        // Check each second if the device is connected.
        // If it's not, reconnect
	    setInterval(()=>{
		    if( this.connected ) return;
			this.connect();
	    },1000);
    }

	connect(){
	// If The named pipe does not exist, do nothing
	if( !existsSync(this.socketPath) ) return;
	// if we're not connected, create the socket
	this.socket = net.createConnection( this.socketPath, ()=>{
		this.connected = true;
		this.emit("connected");
	} );
	// Qcom packets are devided by a newline. On each new line from the socket,
	// emit the data.
		const socketLineIn = readline.createInterface( { terminal : false, input: this.socket } );
		socketLineIn.on('line', (line)=>{
			const obj = JSON.parse(line);
			this.emit("data", obj );
		});
	// When the socket connects, we're connected!
		this.socket.on('connect', ()=>{
			//this.connected = true;
			//this.emit("connected");
	});
		// this.socket.on('error', (err:any)=>{ });
		// this.socket.on('data', function( data:any ){ });
		// this.socket.on('end', ()=>{ });
		// event is called anytime the socket is closed (e.g. error, ...)
		this.socket.on('close', ()=>{
			if(!this.connected) return;
			this.connected = false;
			this.emit("disconnected");
		});
	}
	// Send data to the Qcom device
	send( signal:string, argument:string ){
		const packet = "{"+signal+":"+argument+"}";
		if( this.connected === true ){
			this.socket.write(packet, "utf8");
		}
	}
	// Cleanup function, for when the object
	// is destroyed.
	cleanup(){
		this.socket.destroy();
	}
}
module.exports = Qcom;
