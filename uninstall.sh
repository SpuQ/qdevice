#!/bin/bash

# uninstall.sh
# Remove Qcom Linux Userspace driver and all its
# components from your system.

# check whether script is running as root
if [ "$(id -u)" != "0" ]; then
   echo -e "\e[91mERROR: The uninstall script must be run as root!" 1>&2
   echo -e "\e[0mYou can work around this error by unstalling the Qcom Linux Userspace driver manually. Just execute the uninstall script in the Qcom folder as root user with this command:\\n$ sudo bash /opt/Qcom/uninstall.sh"
   exit 1;
fi

# run Qcom's uninstall script
bash /opt/Qcom/uninstall.sh

# successful exit.
exit 0;
