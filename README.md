# Qdevice
Allow your NodeJS project to interact with the real world through Qcom-based hardware devices. This package uses the [Qcom driver](https://gitlab.com/spuq/qcom "Qcom driver repository"). Qcom is designed for easily connecting low-level hardware (e.g. a LED on a breadboard, wired to your Arduino) to high-level software (e.g. NodeJS).

:warning: **Linux-based systems only! (e.g. your Raspberry Pi, or your Ubuntu workstation)**

## Installation
Install Qdevice with NPM.
```
npm install --save qdevice
```
For a TypeScript project, install the types
```
npm install --save-dev @types/qdevice
```

## How it works
Qcom is a communication layer on top of a serial port interface (ttyUSB/ttyACM/...), designed for easily identifying each device, and easily exchanging information between a serial device and a host system. A Qcom packet looks like this (e.g. coming from/send to your Arduino over serial):
```
{signal:argument}
```
|   |Explanation                        |
|---|----------------------------------------------------|
| { | Start character (packet start)                     |
|signal| Some characters to identify what the data is (e.g. temperature) |
| : | Divider character (to seperate signal from argument)| 
|argument | The data (e.g. 21.5)                        |
| } | Stop character (packet end)                         |

The *Qcom userspace driver* (installed when installing this npm package) is started whenever a new tty* device connects to your host. The *Qcom userspace driver* creates for each Qcom-based device a [named pipe](https://en.wikipedia.org/wiki/Named_pipe) with the device name and the device address. For example, a device named "MySensor" given address 4 will appear in the folder **/Qdev** as:
```
MySensor_4
```
This named pipe can be used to interact with your serial device using any language. But you are here, so you're probably using NodeJS with JavaScript or TypeScript. Great, I already wrote the interface!

## Usage
Create a new Qcom device
```
const mySensor = new Qdevice("MySensor_4");
```
Add event listeners for the device connection status
```
mySensor.on('connected', ()=>{ console.log("Sensor 4 is connected!")});
mySensor.on('disconnected', ()=>{ console.log("Sensor 4 is disconnected!});
```
Listen for incoming data
```
mySensor.on('data', (data)=>{
   if(data.signal === 'temperature'){
      console.log('The temperature is '+data.argument+' °C');
   }
});
```
Send some data to the device
```
mySensor.send('heater','on');
```
That's about it!

## Qcom-based hardware
- [Sensor Module](https://www.tindie.com/products/17452/ "temp/Rhum/Rlight sensor module"): A temperture/relative humidity/relative light sensor
- [Powerswitch Module](https://www.tindie.com/products/17887/ "4-channel power switch"): A 4-channel power switch

## License
This is a project I worked really hard on, but you can use it however you like.
