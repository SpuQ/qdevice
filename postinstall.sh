#!/bin/bash

# postinstall.sh
# script for installing Linux userspace driver for Qcom
# from GitLab repository.

# check whether script is running as root
if [ "$(id -u)" != "0" ]; then
   echo -e "\e[91mERROR: The postinstall script must be run as root!" 1>&2
   echo -e "\e[0mYou can work around this error by installing the Qcom Linux Userspace driver manually. Just execute the postinstall script in this package as root user with this command:\\n$ sudo bash postinstall.sh"
   exit 1;
fi

# Check for Qcom, and download&install Qcom from repository if necessary
echo -ne "Checking for Linux Qcom driver... "
if [ -d /opt/Qcom ]; then
	echo -e "\e[92m installed \e[39m"
else
	echo -e "\e[91m not installed \e[39m"
	echo -e "Installing Linux Qcom driver... \e[39m"
	# because we love meaningless progress bars...
	echo -ne "[##                  ]   \r"
	wget -q -P /tmp/ https://gitlab.com/SpuQ/qcom/-/archive/master/qcom-master.tar
	echo -ne "[########            ]   \r"
	tar -xf /tmp/qcom-master.tar -C /tmp/
	echo -ne "[##############      ]   \r"
	bash /tmp/qcom-master/install.sh > /dev/null
	rm -rf /tmp/qcom-master/ qcom-master.tar
	echo -ne "[####################] "
	echo -e "\e[92mdone\e[39m"
fi

# successful exit!
exit 0;
